const BASE_URL = "https://62db6ccfd1d97b9e0c4f356a.mockapi.io";

function getDSSV() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
    // data:{}
  })
    .then(function (res) {
      tatLoading();
      console.log("res: ", res);
      renderDSSV(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}
getDSSV();
function xoaSinhVien(id) {
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      console.log("res: ", res);
      getDSSV();
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

function themSV() {
  //  newSV: Object lấy từ form
  let newSV = {
    name: "Bánh mì",
    email: "Rosalyn_Rempel@hotmail.com",
    password: "64FGlxrgjW_hgcg",
    math: 85904,
    physics: 66843,
    chemistry: 72923,
  };
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: newSV,
  })
    .then(function (res) {
      tatLoading();
      console.log("res: ", res);
    })
    .catch(function (err) {
      tatLoading();
      console.log("err: ", err);
    });
}
